import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  data: any = '';

  contactForm = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
    pincode: new FormControl('', [
      Validators.required,
      Validators.min(100000),
      Validators.max(999999),
    ]),
    phone: new FormControl('', [
      Validators.required,
      Validators.min(100000000),
      Validators.max(9999999999),
    ]),
  });

  onSubmit() {
    this.data = this.contactForm.value;
    this.contactForm.reset();
  }
}
